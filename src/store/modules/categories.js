import firebase from 'firebase'

export default {
    namespaced: true,
    state: {
        items: {}
    },
    actions: {
        fetchAllCategories ({state, commit}) {
        console.log('\u{1F525}', '\u{1F3F7}', 'all')
        return new Promise((resolve, reject) => {
            firebase.database().ref('categories').once('value', snapshot => {
                const categoriesObject = snapshot.val()
                Object.keys(categoriesObject).forEach(categoryId => {
                    const category = categoriesObject[categoryId]
                    commit('setItem', {resource: 'categories', id: categoryId, item: category}, {root: true})
                })
                resolve(Object.values(state.items))
            })
        })
    },
        fetchCategory: ({dispatch}, {id}) => dispatch('fetchItem', {resource: 'categories', id, emoji: '\u{1F3F7}'}, {root: true}),

        fetchCategories: ({dispatch}, {ids}) =>  dispatch('fetchItems', {resource: 'categories', ids, emoji: '\u{1F3F7}'}, {root: true})
    }
}