import {countObjectProperties} from '@/utils'
import {makeAppendChildToParentMutation} from '@/store/assetHelpers'

export default {
    namespaced: true,
    state: {
        items: {}
    },
    getters: {
        threadsCount: state => id => countObjectProperties(state.items[id].threads)
    },
    actions: {
        fetchForum: ({dispatch}, {id} )=> dispatch('fetchItem', {resource: 'forums', id, emoji: '\u{1F5E8}'}, {root: true}),

        fetchForums: ({dispatch}, {ids}) => dispatch('fetchItems', {resource: 'forums', ids, emoji: '\u{1F6D0}'}, {root: true})
    },
    mutations: {
        appendThreadToForum: makeAppendChildToParentMutation({parent: 'forums', child: 'threads'})
    }
}