import {helpers as vuelidateHelpers} from 'vuelidate/lib/validators'
import firebase from 'firebase'

export const uniqueUsername = (value) => {
    if (!vuelidateHelpers.req(value)) {
        return true
    }
    
    //check in the database if the username is unique
    return new Promise((resolve, reject) => {
        firebase.database().ref('users').orderByChild('usernameLower').equalTo(value.toLowerCase())
        // resolve if the value doesn't exist in the user database
            .once('value', snapshot => resolve(!snapshot.exists()))
    })
}

export const uniqueEmail = (value) => {
    if (!vuelidateHelpers.req(value)) {
        return true
    }

    return new Promise((resolve, reject) => {
        firebase.database().ref('users').orderByChild('email').equalTo(value)
        // resolve if the value doesn't exist in the user database
            .once('value', snapshot => resolve(!snapshot.exists()))

    })
}

export const supportedImageFile = (value) => {
    if (!vuelidateHelpers.req(value)) {
        return true
    }

    const supported = ['jpg', 'jpeg', 'gif', 'png', 'svg']
    // get the suffix of an URL
    const suffix = value.split('.').pop()
    return supported.includes(suffix)
}

export const gotImageResponse = (value) => {
    if (!vuelidateHelpers.req(value)) {
        return true
    }

    return new Promise((resolve, reject) => {
        fetch(value)
            .then(response => resolve(response.ok))
            .catch(() => resolve(false))
    })
}
